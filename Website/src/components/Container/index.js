import React from 'react'
import PropTypes from 'prop-types'

import cx from 'lib/classNames'

import style from './container.module.css'

const Container = ({ component: Component, className, children, noPadding }) => {
  return (
    <Component className={cx(
      style.container,
      noPadding && style.noPadding,
      className
    )}>
      {children}
    </Component>
  )
}

Container.propTypes = {
  component: PropTypes.string,
  className: PropTypes.string,
  children: PropTypes.node,
  noPadding: PropTypes.bool,
}

Container.defaultProps = {
  component: 'div',
  noPadding: false,
}

export default Container
