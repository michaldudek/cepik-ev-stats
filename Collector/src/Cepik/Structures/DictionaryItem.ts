interface DictionaryItem {
  key: string,
  name: string,
  count: number,
}

export default DictionaryItem
