import { isEmptyIndicator } from '@lib/helpers'

const brandAliases: Record<string, string> = {
  'TESLA MOTORS': 'TESLA',
  'BMW I': 'BMW',
}

interface Car {
  id: string,
  brand: string,
  model: string,
  type?: string,
  version: string,
  vehicleType: string,
  fuelType: string,
  firstRegistrationDate: string,
  source: string,
  voivodeshipKey: string,
  voivodeship?: string,
}

function createCarFromCepikData (data: Record<string, any>): Car {
  return {
    id: data.id,
    brand: brandAlias(data.attributes['marka']),
    model: data.attributes['model'],
    type: data.attributes['typ'],
    version: data.attributes['wariant'],
    vehicleType: data.attributes['rodzaj-pojazdu'],
    fuelType: data.attributes['rodzaj-paliwa'],
    firstRegistrationDate: data.attributes['data-pierwszej-rejestracji-w-kraju'],
    source: data.attributes['pochodzenie-pojazdu'],
    voivodeshipKey: data.attributes['wojewodztwo-kod'],
  }
}

/**
 * Alias a brand name (if alias defined).
 * 
 * CEPiK database contains some wrong data about brand names
 * (@see brandAliases const above) and we need to fix that
 * to not split single brand stats into multiple rows.
 * 
 * @param brand Brand name from CEPiK.
 * @return string
 */
function brandAlias (brand: string | undefined | null): string {
  if (!brand || isEmptyIndicator(brand)) {
    return 'N/A'
  }

  return brandAliases[brand]
    ? brandAliases[brand]
    : brand
}

export default Car
export {
  createCarFromCepikData,
}
