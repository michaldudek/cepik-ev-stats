/*
  SVG map by Hiuppo / CC BY-SA (http://creativecommons.org/licenses/by-sa/3.0/)
  From WikiMedia Commons:
    @see https://commons.wikimedia.org/wiki/File:POL_location_map.svg
*/
import React, { useState } from 'react'
import PropTypes from 'prop-types'

import cx from 'lib/classNames'

import Tooltip from './Tooltip'
import Voivodeship from './Voivodeship'
import { voivodeships } from './config'

import style from './polandMap.module.css'

export { normalizeName } from './helpers'

const PolandMap = ({ className, data, onClick }) => {
  const [ mouseover, setMouseover ] = useState(false)
  const [ mouseoverVoivodeshipId, setMouseoverVoivodeshipId ] = useState()
  const [ tooltipPosition, setTooltipPosition ] = useState({})

  const handleMouseOver = (event, id) => {
    const rect = event.currentTarget.getBoundingClientRect()
    const position = {
      top: rect.y + (rect.height / 2),
      left: rect.left + (rect.width / 2),
    }
    setTooltipPosition(position)
    setMouseoverVoivodeshipId(id)
  }

  const mouseoverVoivodeshipData = mouseoverVoivodeshipId
    ? {
        id: mouseoverVoivodeshipId,
        name: voivodeships[mouseoverVoivodeshipId].name,
        ...data[mouseoverVoivodeshipId],
      }
    : null

  return (
    <div
      className={cx(style.root, className)}
      onMouseEnter={() => setMouseover(true)}
      onMouseLeave={() => setMouseover(false)}
    >
      <svg
        className={style.map}
        version='1.0'
        xmlns='http://www.w3.org/2000/svg'
        viewBox='0 0 497 463'
      >
        {Object.keys(voivodeships).map((key) => {
          const cfg = data[key] || {}
          return (
            <Voivodeship
              key={key}
              id={key}
              name={voivodeships[key].name}
              path={voivodeships[key].path}
              label={cfg.label}
              color={cfg.color}
              onClick={onClick}
              onMouseOver={handleMouseOver}
            />
          )
        })}
      </svg>
      {mouseover && mouseoverVoivodeshipData && (
        <Tooltip
          position={tooltipPosition}
          name={mouseoverVoivodeshipData.name}
          label={mouseoverVoivodeshipData.label}
          color={mouseoverVoivodeshipData.color}
        />
      )}
    </div>
  )
}

export default PolandMap

const VoivodeshipDataShape = PropTypes.shape({
  label: PropTypes.string,
  color: PropTypes.string,
})

PolandMap.propTypes = {
  className: PropTypes.string,
  data: PropTypes.shape({
    podkarpackie: VoivodeshipDataShape,
    malopolskie: VoivodeshipDataShape,
    slaskie: VoivodeshipDataShape,
    opolskie: VoivodeshipDataShape,
    dolnoslaskie: VoivodeshipDataShape,
    swietokrzyskie: VoivodeshipDataShape,
    lubelskie: VoivodeshipDataShape,
    lodzkie: VoivodeshipDataShape,
    mazowieckie: VoivodeshipDataShape,
    wielkopolskie: VoivodeshipDataShape,
    lubuskie: VoivodeshipDataShape,
    kujawskopomorskie: VoivodeshipDataShape,
    podlaskie: VoivodeshipDataShape,
    zachodniopomorskie: VoivodeshipDataShape,
    warminskomazurskie: VoivodeshipDataShape,
    pomorskie: VoivodeshipDataShape,
  }),
  onClick: PropTypes.func,
}

PolandMap.defaultProps = {
  data: {},
  onClick: () => null,
}
