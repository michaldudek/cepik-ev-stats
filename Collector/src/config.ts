import moment from 'moment'

import Car from './Car'

export const FORMAT_YEAR = 'YYYY'
export const FORMAT_QUARTER = 'YYYY[Q]Q'
export const FORMAT_MONTH = 'YYYY[M]MM'
export const FORMAT_DAY = 'YYYYMMDD'
export const FORMAT_CEPIK = 'YYYY-MM-DD'

export const VEHICLE_TYPE_CAR = 'SAMOCHÓD OSOBOWY'
export const VEHICLE_TYPE_MOPED = 'MOTOROWER'
export const VEHICLE_TYPE_MOTORCYCLE = 'MOTOCYKL'
export const VEHICLE_TYPE_TRUCK = 'SAMOCHÓD CIĘŻAROWY'
export const VEHICLE_TYPE_BUS = 'AUTOBUS'
export const VEHICLE_TYPE_SPECIAL = 'SAMOCHÓD SPECJALNY'
export const VEHICLE_TYPE_OTHER = 'SAMOCHODOWY INNY'

export interface TimePeriodConfig {
  key: string,
  type: 'y' | 'Q' | 'M',
  filter: Function,
}

export interface DataSetConfig {
  key: string,
  type: 'noFilter' | 'vehicleType' | 'date',
  filter?: Function,
  stats: Array<string>,
}

const basicStats = [
  'brands',
  'models',
  'sources',
  'voivodeships',
]

const timeStats = [
  'registrationsByYear',
  'registrationsByQuarter',
  'registrationsByMonth',
]

/**
 * Create config.
 *
 * @param startDate Start date for periods to be generated.
 */
export default function (startDate = '20100101') {
  const datasets: Array<DataSetConfig> = [
    {
      key: 'all',
      type: 'noFilter',
      stats: [
        ...basicStats,
        ...timeStats,
        'vehicleTypes',
      ],
    },
    // split by vehicle type
    {
      key: 'cars',
      type: 'vehicleType',
      filter: (car: Car) => car.vehicleType === VEHICLE_TYPE_CAR,
      stats: [
        ...basicStats,
        ...timeStats,
      ],
    }, 
    {
      key: 'mopeds',
      type: 'vehicleType',
      filter: (car: Car) => car.vehicleType === VEHICLE_TYPE_MOPED,
      stats: [
        ...basicStats,
        ...timeStats,
      ],
    },
    {
      key: 'motorcycles',
      type: 'vehicleType',
      filter: (car: Car) => car.vehicleType === VEHICLE_TYPE_MOTORCYCLE,
      stats: [
        ...basicStats,
        ...timeStats,
      ],
    },
    {
      key: 'trucks-buses',
      type: 'vehicleType',
      filter: (car: Car) => car.vehicleType === VEHICLE_TYPE_TRUCK ||
        car.vehicleType === VEHICLE_TYPE_BUS,
      stats: [
        ...basicStats,
        ...timeStats,
      ],
    },
    {
      key: 'others',
      type: 'vehicleType',
      filter: (car: Car) => car.vehicleType === VEHICLE_TYPE_SPECIAL ||
        car.vehicleType === VEHICLE_TYPE_OTHER,
      stats: [
        ...basicStats,
        ...timeStats,
      ],
    }
  ]

  // generate time periods
  const since = moment(startDate, FORMAT_DAY)
  const periods = [
    ...timePeriodsUntilNow(since, 'y', FORMAT_YEAR),
    ...timePeriodsUntilNow(since, 'Q', FORMAT_QUARTER),
    // ...timePeriodsUntilNow(since, 'M', FORMAT_MONTH),
  ]

  // create dataset for each generated period
  periods.forEach((period: TimePeriodConfig) => {
    datasets.push({
      key: period.key,
      type: 'date',
      filter: period.filter,
      stats: [ ...basicStats ],
    })
  })

  return {
    periods,
    datasets,
  }
}

/**
 * Generate time periods config from the start date until now.
 *
 * @param since Start date.
 * @param period Years, quarters or months?
 * @param keyFormat moment.js format for keys.
 */
function timePeriodsUntilNow (
  since: moment.Moment,
  period: 'y' | 'Q' | 'M',
  keyFormat: string
): Array<TimePeriodConfig> {
  const now = moment()
  const current = since.clone().startOf(period)
  const periods = []

  while (current.isBefore(now)) {
    const key = current.format(keyFormat)
    
    periods.push({
      key,
      type: period,
      filter: (car: Car) => {
        const regDate = moment(car.firstRegistrationDate, FORMAT_CEPIK)
        return regDate.format(keyFormat) === key
      },
    })
    
    current.add(1, period).startOf(period)
  }

  return periods
}