import React from 'react'
import PropTypes from 'prop-types'

import cx from 'lib/classNames'

import style from './toggle.module.css'

const Toggle = ({ className, onChange, value, label, defaultChecked }) => {
  return (
    <label className={cx(style.container, className)}>
      <input
        className={style.input}
        type="checkbox"
        value={value}
        defaultChecked={defaultChecked}
        onChange={onChange}
      />
      {label}
    </label>
  )
}

export default Toggle

Toggle.propTypes = {
  className: PropTypes.string,
  onChange: PropTypes.func,
  label: PropTypes.string,
  defaultChecked: PropTypes.bool,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ])
}

Toggle.defaultProps = {
  onChange: () => null,
  defaultChecked: false,
}
