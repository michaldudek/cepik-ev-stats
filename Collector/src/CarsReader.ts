import { Readable } from 'stream'
import moment from 'moment'

import { FORMAT_DAY } from '@config'

import cepik, { Voivodeship } from './Cepik'
import Car, { createCarFromCepikData } from './Car'

export default class CarsReader extends Readable
{
  private queue: Array<FetchQueueItem> = []

  private count = 0

  readonly promise: Promise<number>

  constructor (
    startDate = '20100101',
    step: 'y' | 'Q' | 'M' | 'w' = 'y'
  ) {
    super({objectMode: true })

    // prepare a queue of requests to be made
    const now = moment()
    let current = moment(startDate, FORMAT_DAY)
    let until = moment()

    // fire separate requests for every time period
    while (current.isBefore(now)) {
      until = current.clone().endOf(step)

      cepik.getVoivodeships().map((voivodeship: Voivodeship) => {
        this.queue.push({ voivodeship, current, until })
      })

      current = current.clone().add(1, step).startOf(step)
    }

    this.promise = new Promise((resolve, reject) => {
      this.on('end', () => resolve(this.count))
      this.on('error', reject)
    })
  }

  async _read () {
    let cars: Array<Car> = []
    let queueItem: FetchQueueItem | undefined

    // fire a request from queue for every read step in the stream
    while (cars.length === 0 && (queueItem = this.queue.shift())) {
      cars = await this.fetchCarsForVoivodeship(
        queueItem.voivodeship,
        queueItem.current,
        queueItem.until
      )
      cars.forEach((car: Car) => this.push(car))

      this.count += cars.length
    }

    // if queue is empty then notify the stream about it
    if (this.queue.length === 0) {
      console.log(`Finished retrieving ${this.count} registration records.`)
      this.push(null)
    }
  }

  /**
   * Fetch cars registered in the given voivodeship and the given
   * time period.
   *
   * @param voivodeship Voivodeship to fetch cars from.
   * @param fromDate From what date to fetch the cars?
   * @param toDate Until what date to fetch the cars?
   */
  private async fetchCarsForVoivodeship (
    voivodeship: Voivodeship,
    fromDate: moment.Moment,
    toDate?: moment.Moment
  ) {
    const registrations = await cepik.fetchRegistrations(
      voivodeship,
      fromDate,
      toDate
    )

    console.log(`      Fetched ${registrations.length} entries in ${voivodeship.name} for period starting ${fromDate.format(FORMAT_DAY)}`)

    return registrations.map((data: Record<string, any>) => ({
      ...createCarFromCepikData(data),
      voivodeship: voivodeship.name,
    }))
  }
}

interface FetchQueueItem {
  voivodeship: Voivodeship,
  current: moment.Moment,
  until?: moment.Moment,
}
