import React from 'react'
import PropTypes from 'prop-types'

const ExtLink = ({ to, children }) => (
  <a
    href={to}
    target='_blank'
    rel='noopener noreferrer'
  >{children}</a>
)

export default ExtLink

ExtLink.propTypes = {
  to: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
}
