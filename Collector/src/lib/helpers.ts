import fs from 'fs'
import path from 'path'
import mkdirp from 'mkdirp'
import stringifySync from 'csv-stringify/lib/sync'

/**
 * CEPiK has different ways of presenting a blank field
 * and we need to check that.
 *
 * @param s 
 * @return boolean
 */
export function isEmptyIndicator (s: string | undefined | null): boolean {
  return !s ||
    s === '' ||
    s === undefined ||
    s === null ||
    s === '---' ||
    s === 'null'
}

/**
 * Write object data to a JSON file.
 * 
 * Returns the JSON.
 *
 * @param filePath Target path.
 * @param data Data to be saved.
 * @return string
 */
export function writeJson (filePath: string, data: any) {
  const json = JSON.stringify(data, null, 2)
  fs.writeFileSync(
    ensurePath(resolvePath(filePath)),
    json
  )
  return json
}

/**
 * Write collection to a CSV file.
 * 
 * Returns the CSV.
 *
 * @param filePath Target path.
 * @param data Data to be saved.
 * @param header Should headers be generated based on keys?
 * @return string
 */
export function writeCsv (filePath: string, data: Array<any>, header = true) {
  const csv = stringifySync(data, { header })
  fs.writeFileSync(
    ensurePath(resolvePath(filePath)),
    csv
  )
  return csv
}

/**
 * Make sure given path exists.
 * 
 * Returns the path.
 *
 * @param filePath File path.
 * @return string
 */
export function ensurePath (filePath: string) {
  const dirPath = path.dirname(filePath)

  if (!fs.existsSync(dirPath)) {
    mkdirp.sync(dirPath)
  }

  return filePath
}

/**
 * Resolve relative path to absolute path.
 *
 * @param relativePath Relative path to be resolved.
 * @return string
 */
export function resolvePath (relativePath: string) {
  const rootDir = fs.realpathSync(process.cwd())
  return path.resolve(rootDir, relativePath)  
}

/**
 * Safely delete a file.
 *
 * @param filePath File to be removed.
 */
export function unlink (filePath: string) {
  try {
    fs.unlinkSync(filePath)
  } catch(err) {}
}
