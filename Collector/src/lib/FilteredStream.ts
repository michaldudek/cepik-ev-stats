import { Transform } from 'stream'

import Car from '@root/Car'

const noFilter = () => true

export default class FilteredStream extends Transform
{
  readonly promise: Promise<null>

  private readonly filterFn: Function

  constructor (filterFn?: Function) {
    super({ objectMode: true })

    this.filterFn = filterFn || noFilter

    this.promise = new Promise((resolve, reject) => {
      this.on('finish', resolve)
      this.on('error', reject)
    })
  }

  _transform (car: Car, encoding: string, next: Function) {
    if (this.filterFn(car)) {
      this.push(car)
    }
    next()
  }
}

export function filterStream (fn?: Function) {
  return new FilteredStream(fn)
}
