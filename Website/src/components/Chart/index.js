import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { statTypes } from 'config/strings'
import cx from 'lib/classNames'
import readCsv from 'lib/readCsv'

import Button from 'components/Button'
import Brands from './Brands'
import InTime from './InTime'
import Models from './Models'
import Sources from './Sources'
import VehicleTypes from './VehicleTypes'
import Voivodeships from './Voivodeships'

import style from './chart.module.css'

const typeToChartMap = {
  brands: Brands,
  models: Models,
  versions: null,
  vehicleTypes: VehicleTypes,
  registrationsByYear: InTime,
  registrationsByQuarter: InTime,
  registrationsByMonth: InTime,
  sources: Sources,
  voivodeships: Voivodeships,
}

const Chart = ({ className, type, fileUrl, label, ...passProps }) => {
  const [ data, setData ] = useState()

  useEffect(() => {
    const fetchData = async () => {
      const res = await fetch(fileUrl)
      const text = await res.text()
      const data = readCsv(text)
      setData(data)
    }
    fetchData()
  }, [fileUrl])

  const ChartComponent = typeToChartMap[type]

  return (
    <figure className={cx(style.container, className)}>
      <figcaption className={style.title}>
        {statTypes[type]}
        <small className={style.subtitle}>{label}</small>
      </figcaption>
      {data && data.length > 0 && ChartComponent && (
        <>
          <Button
            className={style.dataDownloadButton}
            to={fileUrl}
            icon='download'
            external
          >
            CSV
          </Button>
          <ChartComponent
            data={data}
            {...passProps}
          />
        </>
      )}
      {(!data || data.length === 0) && (
        <div className={style.noData}>
          <FontAwesomeIcon
            icon='chart-pie'
            className={style.noDataIcon}
          />
          <p className={style.noDataLabel}>Brak danych</p>
        </div>
      )}
    </figure>
  )
}

export default Chart

Chart.propTypes = {
  className: PropTypes.string,
  type: PropTypes.string.isRequired,
  fileUrl: PropTypes.string.isRequired,
  label: PropTypes.string,
}
