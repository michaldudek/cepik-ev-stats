export default {
  countLabel: 'Liczba rejestracji',
  hideNiche: (cutOff) => `Schowaj niszowe (< 0,2% rynku [${cutOff} szt.])`,
}

export const vehicleTypes = {
  'all': 'Wszystkie pojazdy',
  'cars': 'Samochody osobowe',
  'mopeds': 'Motorowery',
  'motorcycles': 'Motocykle',
  'trucks-buses': 'Samochody ciężarowe i autobusy',
  'others': 'Inne',
}

export const statTypes = {
  brands: 'Najczęściej rejestrowane marki',
  models: 'Najczęściej rejestrowane modele',
  versions: 'Najczęściej rejestrowane wersje',
  vehicleTypes: 'Najczęściej rejestrowane typy pojazdów',
  registrationsByYear: 'Rejestracje wg lat',
  registrationsByQuarter: 'Rejestracje wg kwartałów',
  registrationsByMonth: 'Rejestracje wg miesięcy',
  sources: 'Źródła najczęściej rejestrowanych pojazdów',
  voivodeships: 'Rejestracje pojazdów wg województw',
}
