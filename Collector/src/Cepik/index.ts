import axios, { AxiosResponse } from 'axios'
import Bottleneck from 'bottleneck'
import * as moment from 'moment'

import Timer from '@lib/Timer'

import CepikDictionaryItem from './Structures/CepikDictionaryItem'
import CepikResponse from './Structures/CepikResponse'
import FuelType from './Structures/FuelType'
import Voivodeship from './Structures/Voivodeship'

/**
 * CEPiK API client tailored to getting EV registrations.
 * 
 * Has throttling built in to make sure calling the API
 * doesn't hit the limits.
 */
class Cepik
{
  readonly apiUrl = 'https://api.cepik.gov.pl/'

  private fuelTypes: Array<FuelType> = []
  private voivodeships: Array<Voivodeship> = []
  // vehicleTypes: 'rodzaj-pojazdu'
  // vehicleSources: 'pochodzenie-pojazdu'
  // brands: 'marka'

  /**
   * Make CEPiK API throttled call, but make sure we don't
   * hit the limits.
   * 
   * The request itself is wrapped in Bottleneck and will
   * execute at most 100 times per minute.
   * 
   * NOTE: Actually, after testing it turns out that CEPiK API
   * gets really slow after hammering it with requests,
   * so I'm greatly lowering the speed of fetching.
   * 
   * @param url URL to query.
   * @param attempt Attempt number, for retrying requests if needed.
   */
  request: Function = (new Bottleneck({ maxConcurrent: 1, minTime: 60 / 12 })) // 12 req / 1 min -> 1 req per 5 sec
    .wrap(async (url: string) => {
      const requestUrl = url.startsWith('https://') || url.startsWith('http://')
        ? url
        : this.apiUrl + url

      const res = await this._get(requestUrl, 0, 5)
      return res.data
    })

  /**
   * Fetch some meta data from CEPiK, mainly from dictionaries.
   */
  async fetchMetaData () {
    return await Promise.all([
      this.fetchVoivodeships(),
      this.fetchFuelTypes(),
    ])
  }

  async fetchRegistrations (
    voivodeship: Voivodeship,
    fromDate: moment.Moment,
    toDate?: moment.Moment
  ) {
    const params: Record<string, string> = {
      'wojewodztwo': voivodeship.key,
      'filter[rodzaj-paliwa]': 'ENERGIA ELEKTRYCZNA',
    }

    params['data-od'] = fromDate.format('YYYYMMDD')

    if (toDate) {
      params['data-do'] = toDate.format('YYYYMMDD')
    }

    const paramsString = Object.keys(params)
      .map((key) => `${key}=${encodeURIComponent(params[key])}`)
      .join('&')

    const data = await this.paginatedRequest(
      await this.request(`pojazdy?${paramsString}`)
    )
    return data
  }

  private async paginatedRequest (response: CepikResponse) {
    let data = [ ...response.data ]

    if (response.links.next) {
      const nextData = await this.paginatedRequest(
        await this.request(response.links.next)
      )
      data = [ ...data, ...nextData ]
    }

    return data
  }

  /* META DATA FETCHERS */

  private async fetchDictionary (name: string) {
    const res = await this.request(`slowniki/${name}`)

    return res.data.attributes['dostepne-rekordy-slownika']
      .map((item: CepikDictionaryItem) => ({
        key: item['klucz-slownika'],
        name: item['wartosc-slownika'],
        count: item['liczba-wystapien'],
      }))
  }

  private async fetchVoivodeships () {
    this.voivodeships = await this.fetchDictionary('wojewodztwa')
    return this.voivodeships
  }


  private async fetchFuelTypes () {
    this.fuelTypes = await this.fetchDictionary('rodzaj-paliwa')
    return this.fuelTypes
  }

  /**
   * Actually perform GET request on the given URL and retry if it fails (without waiting).
   * 
   * @param url URL to GET.
   * @param attempt Attempt number.
   * @param maxAttempts Max number of allowed attempts.
   */
  private async _get (url: string, attempt: number = 0, maxAttempts: number = 5): Promise<AxiosResponse> {
    const timer = new Timer()
    console.log(`  GET (${attempt}) ${url}`)

    try {
      return await axios.get(url)
    } catch (error) {
      if (error && error.response) {
        console.log(`      HTTP ${error.response.status} ${timer.stop()} ms`)
      } else if (error) {
        console.log(`      ERROR: ${error.message} ${timer.stop()} ms`)
      } else {
        console.log(`      ERROR: unknown ${timer.stop()} ms`)
      }

      // retry the request 5 times and if it fails then rethrow
      if (attempt > maxAttempts) {
        console.log(`      Retried ${attempt} times, giving up.`)
        throw error
      }

      console.log(`      Retry: ${attempt + 1}`)
      return await this._get(url, attempt + 1, maxAttempts)
    }
  }

  /* GETTERS */
  getVoivodeships () {
    return this.voivodeships
  }

  getFuelTypes () {
    return this.fuelTypes
  }
}

export default new Cepik()

export {
  Voivodeship,
  Cepik,
}
