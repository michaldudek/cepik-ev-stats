import React from 'react'
import PropTypes from 'prop-types'
import { Bar } from 'react-chartjs-2'

import strings from 'config/strings'
import { DataShape } from 'config/types'
import { COLOR_RED } from 'config/colors'

const InTime = ({ data }) => {
  const labels = []
  const dataPoints = []

  data.forEach((item) => {
    labels.push(item.key)
    dataPoints.push(item.count)
  })

  return (
    <Bar
      data={{
        labels: labels,
        datasets: [{
          label: strings.countLabel,
          backgroundColor: COLOR_RED,
          data: dataPoints,
        }]
      }}
      legend={{ display: false }}
      options={{
        scales: {
          xAxes: [
            {
              gridLines: {
                drawOnChartArea: false,
              }
            }
          ],
          yAxes: [
            {
              ticks: {
                min: 0,
              }
            }
          ]
        }
      }}
    />
  )
}

export default InTime

InTime.propTypes = {
  data: PropTypes.arrayOf(DataShape).isRequired,
}
