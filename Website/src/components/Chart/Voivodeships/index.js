import React from 'react'
import PropTypes from 'prop-types'

import strings from 'config/strings'
import { DataShape } from 'config/types'
import { SHADES_RED, getRedShadeForNumber } from 'config/colors'

import PolandMap, { normalizeName } from 'components/PolandMap'

import style from './voivodeships.module.css'

const Voivodeships = ({ data }) => {
  const max = data.reduce((max, { count }) => Math.max(max, count), 0)
  const step = Math.ceil(max / SHADES_RED.length)

  let current = 0
  const intervals = []
  while (current <= max) {
    intervals.push({
      min: current,
      max: current + step - 1,
      color: getRedShadeForNumber(intervals.length)
    })
    current += step
  }
  
  const mapData = {}
  data.forEach(({ key, count }) => {
    const name = normalizeName(key)
    if (!name) {
      return
    }

    const interval = Math.ceil(count / step) - 1
    const color = getRedShadeForNumber(interval)

    mapData[name] = {
      color: color,
      label: `${strings.countLabel}: ${count}`,
    }
  })

  return (
    <>
      <ul className={style.legend}>
        {intervals.map(({ min, max, color }) => (
          <li  className={style.interval} key={min}>
            <div className={style.color} style={{ backgroundColor: color }} />
            {min}-{max}
          </li>
        ))}
      </ul>
      <PolandMap data={mapData} />
    </>
  )
}

export default Voivodeships

Voivodeships.propTypes = {
  data: PropTypes.arrayOf(DataShape).isRequired,
}
