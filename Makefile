.PHONY: dev dev-data run

install:
	cd Website && npm i
	cd Collector && npm i

dev:
	cd Website && npm start

# collect data so there's at least 2 years, 2 quarters and 2 months
dev-data:
	cd Collector && npm start -- --from=20190101 --output-dir=../Website/public/data/

run: dev-data dev
