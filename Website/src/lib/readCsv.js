import Papa from  'papaparse'

export default function (text) {
  const { data } = Papa.parse(text, { header: true })

  // might need to remove last invalid element
  if (data && data.length > 0) {
    const lastElement = data.pop()
    if (lastElement.key) {
      data.push(lastElement)
    }
  }

  return data
}
