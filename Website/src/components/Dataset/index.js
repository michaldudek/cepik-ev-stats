import React from 'react'

import { DataSetInfoShape } from 'config/types'
import { vehicleTypes } from 'config/strings'
import { DATA_DIR } from 'config'

import Button from 'components/Button'
import Chart from 'components/Chart'

import style from './dataset.module.css'

const chartsOrder = [
  'brands',
  'models',
  // 'versions',
  'registrationsByYear',
  'registrationsByQuarter',
  'registrationsByMonth',
  'vehicleTypes',
  'sources',
  'voivodeships',
]

const Dataset = ({ dataset }) => {
  let title = ''
  switch (dataset.type) {
    case 'noFilter':
      title = vehicleTypes.all
      break
    case 'vehicleType':
      title = vehicleTypes[dataset.key]
      break
    case 'date':
      title = `Dane za okres: ${dataset.key}`
      break
    default:
      title = ''
  }

  // display stats in pre-defined order, not how they appear in dataset
  const stats = []
  chartsOrder.forEach((type) => {
    const chart = dataset.stats.find(({ key }) => key === type)
    if (chart) {
      stats.push(chart)
    }
  })

  const hasVehicleTypes = !!dataset.stats.find(({ key }) => key === 'vehicleTypes')
  const hasSources = !!dataset.stats.find(({ key }) => key === 'sources')

  return (
    <article>
      <header className={style.header}>
        <h2 className={style.title}>{title}</h2>
        <Button
          to={DATA_DIR + dataset.dataFile}
          icon='download'
          external
        >
          Dane źródłowe (CSV)
        </Button>
      </header>
      <section className={style.charts}>
        {stats.map(({ key, file }) => (
          <Chart
            key={key}
            className={style[key]}
            type={key}
            label={title}
            fileUrl={DATA_DIR + file}
            size={
              (key === 'vehicleTypes' || key === 'sources') &&
                hasVehicleTypes && hasSources
                ? 'half'
                : 'full'
            }
          />
        ))}
      </section>
    </article>
  )
}

export default Dataset

Dataset.propTypes = {
  dataset: DataSetInfoShape.isRequired,
}
