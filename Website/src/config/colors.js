export const COLOR_RED = '#f44336'
export const COLOR_BLUE = '#007bff'
export const COLOR_INDIGO = '#6610f2'
export const COLOR_PURPLE = '#6f42c1'
export const COLOR_PINK = '#e83e8c'
export const COLOR_ORANGE = '#fd7e14'
export const COLOR_YELLOW = '#ffc107'
export const COLOR_GREEN = '#28a745'
export const COLOR_TEAL = '#20c997'
export const COLOR_CYAN = '#17a2b8'

export const COLORS = [
  COLOR_RED,
  COLOR_ORANGE,
  COLOR_PINK,
  COLOR_YELLOW,
  COLOR_CYAN,
  COLOR_BLUE,
  COLOR_INDIGO,
  COLOR_PURPLE,
  COLOR_TEAL,
  COLOR_GREEN,
]

export const SHADES_RED = [
  '#b71c1c',
  '#c62828',
  '#d32f2f',
  '#e53935',
  '#f44336',
  '#ef5350', 
  '#ef9a9a',
  '#ffcdd2',
].reverse()

export function getColorForNumber (num) {
  return COLORS[num % COLORS.length]
}

export function getRedShadeForNumber (num) {
  return SHADES_RED[num % SHADES_RED.length]
}
