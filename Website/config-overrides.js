const reactAppRewirePostcss = require('react-app-rewire-postcss');
const postcssCustomMedia = require('postcss-custom-media');
const postcssNesting = require('postcss-nesting');

module.exports = config => reactAppRewirePostcss(config, {
  plugins: () => [
    postcssCustomMedia({
      importFrom: 'src/styles/media.css',
    }),
    postcssNesting(),
  ]
});
