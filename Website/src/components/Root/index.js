import React, { useEffect, useState } from 'react'
import { Switch, Route, Redirect, useLocation } from 'react-router-dom'

import PageHeader from 'components/PageHeader'
import PageNav from 'components/PageNav'
import PageFooter from 'components/PageFooter'

import About from 'pages/About'
import Stats from 'pages/Stats'
import Links from 'pages/Links'

export default () => {
  const [ meta, setMeta ] = useState()
  const [ error, setError ] = useState(false)

  // get meta information
  useEffect(() => {
    const fetchMeta = async () => {
      try {
        const res = await fetch(process.env.PUBLIC_URL + '/data/meta.json')
        const json = await res.json()
        setMeta(json)
      } catch (error) {
        console.error(error)
        setError(true)
      }
    }
    fetchMeta()
  }, [])

  // track pageviews in GA
  const location = useLocation()
  useEffect(() => {
    window.gtag('config', 'UA-2364455-14', { page_path: location.pathname })
  }, [ location ])

  return (
    <>
      <PageHeader updated={meta ? meta.date : null} />
      <PageNav />
      <div className='page-wrap'>
        <Switch>
          <Route path='/stats'>
            <Stats datasets={meta ? meta.datasets : []} error={error} />
          </Route>
          <Route path='/links'>
            <Links />
          </Route>
          <Route path='/about'>
            <About />
          </Route>
          <Route path='/'>
            <Redirect to='/stats' />
          </Route>
        </Switch>
      </div>
      <PageFooter updated={meta ? meta.date : null} />
    </>
  )
}
