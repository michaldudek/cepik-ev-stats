import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import cx from 'lib/classNames'

import style from './button.module.css'

const Button = ({ children, className, icon, to, type, external }) => {
  const buttonProps = {}

  let Component = 'button'
  if (to && external) {
    Component = 'a'
    buttonProps.href = to
    buttonProps.target = '_blank'
  } else if (to) {
    Component = Link
    buttonProps.to = to
  } else {
    buttonProps.type = type
  }

  return (
    <Component
      className={cx(style.button, className)}
      {...buttonProps}
    >
      {icon && <FontAwesomeIcon icon={icon} className={style.icon} />}
      {children}
    </Component>
  )
}

export default Button

Button.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  external: PropTypes.bool,
  icon: PropTypes.string,
  to: PropTypes.string,
  type: PropTypes.oneOf(['submit', 'button'])
}

Button.defaultProps = {
  external: false,
  type: 'button'
}
