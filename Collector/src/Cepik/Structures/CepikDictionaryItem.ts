export default interface CepikDictionaryItem {
  'klucz-slownika': string,
  'wartosc-slownika': string,
  'liczba-wystapien': number,
}
