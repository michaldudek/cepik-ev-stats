const aliases = {
  malopolskie: ['małopolskie'],
  slaskie: ['śląskie'],
  dolnoslaskie: ['dolnośląskie'],
  swietokrzyskie: ['świętokrzyskie'],
  lodzkie: ['łódzkie'],
  kujawskopomorskie: ['kujawsko-pomorskie'],
  zachodniopomorskie: [
    'zachodnio-pomorskie',
    'zachodnio pomorskie',
    'zach. pomorskie',
    'zach pomorskie',
  ],
  warminskomazurskie: [
    'warmińskomazurskie',
    'warmińsko-mazurskie',    
  ],
}

export function normalizeName (name) {
  name = name.toLowerCase().trim()

  if (name.startsWith('woj.')) {
    name = name.substr(4).trim()
  }

  let alias = null
  Object.keys(aliases).forEach((key) => {
    if (aliases[key].indexOf(name) > -1) {
      alias = key
    }
  })

  return alias || name
}
