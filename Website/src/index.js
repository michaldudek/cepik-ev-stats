import React from 'react'
import ReactDOM from 'react-dom'
import { HashRouter as Router } from 'react-router-dom'
import { library as faLibrary } from '@fortawesome/fontawesome-svg-core'
import { faDownload, faChartPie, faExclamationTriangle } from '@fortawesome/free-solid-svg-icons'
import 'normalize.css'

import Root from 'components/Root'

import * as serviceWorker from './serviceWorker'

import 'styles/variables.css'
import 'styles/global.css'

// optimize bundle size by allowing only specific FA icons
faLibrary.add(faDownload, faChartPie, faExclamationTriangle)

ReactDOM.render(
  <React.StrictMode>
    <Router hashType='hashbang'>
      <Root />
    </Router>
  </React.StrictMode>,
  document.getElementById('root')
);

serviceWorker.unregister()
