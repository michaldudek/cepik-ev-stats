import fs from 'fs'
import { PassThrough, Writable } from 'stream'
import stringify from 'csv-stringify'

import { resolvePath, unlink, ensurePath } from '@lib/helpers'

export default class CsvWriter extends PassThrough
{
  private readonly realPath: string

  private readonly stringifier: stringify.Stringifier

  private readonly fileStream: Writable

  readonly promise: Promise<any>

  constructor (filePath: string, overwrite = true) {
    super({ objectMode: true })

    // handle path and make sure it exists
    this.realPath = resolvePath(filePath)
    ensurePath(this.realPath)

    if (overwrite) {
      unlink(this.realPath)
    }

    // create substreams
    this.stringifier = stringify({ header: overwrite })
    this.fileStream = fs.createWriteStream(this.realPath)

    // connect all together
    this.pipe(this.stringifier)
    this.stringifier.pipe(this.fileStream)

    this.promise = Promise.all([
      new Promise((resolve, reject) => {
        this.on('end', resolve)
        this.on('error', reject)
      }),
      new Promise((resolve, reject) => {
        this.fileStream.on('finish', resolve)
        this.fileStream.on('error', reject)
      }),
      new Promise((resolve, reject) => {
        this.stringifier.on('finish', resolve)
        this.stringifier.on('error', reject)
      })
    ])
  }
}
