import PropTypes from 'prop-types'

export const StatInfoShape = PropTypes.shape({
  key: PropTypes.string.isRequired,
  file: PropTypes.string.isRequired,
})

export const DataSetInfoShape = PropTypes.shape({
  key: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  dataFile: PropTypes.string.isRequired,
  stats: PropTypes.arrayOf(StatInfoShape).isRequired,
})

export const DataShape = PropTypes.shape({
  key: PropTypes.string.isRequired,
  count: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
  ]).isRequired,
})
