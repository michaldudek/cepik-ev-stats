import React from 'react'

import Container from 'components/Container'
import ExtLink from 'components/ExtLink'

import style from 'styles/contentPage.module.css'

export default () => (
  <Container className={style.page}>
    <h2>Informacje</h2>

    <h3>Co to za strona i po co powstała?</h3>
    <p><i>CEPiK EV Stats</i> to statystyki sprzedaży (a właściwie rejestracji) pojazdów elektrycznych w Polsce.</p>
    <p>Celem tej strony jest informowanie o stanie rynku pojazdów elektrycznych w Polsce, przede wszystkim liczbie pojazdów zarejestrowanych i dopuszczonych do ruchu (w domyśle: sprzedanych), a także o tym jakie są najpopularniejsze marki, modele i segmenty.</p>
    <p>Powstała jako hobbystyczny projekt "po godzinach" i nie ma na celu żadnej komercyjnej działalności.</p>
    
    <h3>Skąd pochodzą dane?</h3>
    <p>Dane pochodzą z <ExtLink to='http://www.cepik.gov.pl/'>Centralnej Ewidencji Pojazdów i Kierowców</ExtLink>, która udostępnia swoją (zanonimizowaną) bazę danych poprzez <ExtLink to='https://api.cepik.gov.pl/doc'>publiczne API</ExtLink>.</p>
    
    <h3>Jak często aktualizowane są dane?</h3>
    <p>Dane w API CEPiK aktualizowane są co tydzień, a ta strona gromadzi dane z API CEPiK również co tydzień.</p>
    <p>Niestety nie znalazłem informacji dokładnie w jaki dzień następuje aktualizacja danych w CEPiK dlatego aktualizacja strony może nie być idealnie zsynchronizowana z aktualizacją CEPiK.</p>

    {/* <h3>Jak to działa?</h3> */}

    <h3>Czy mogę wykorzystać te dane do innych opracowań (w tym komercyjnie)?</h3>
    <p>Tak.</p>
    <p>Wszystkie udostępniane tutaj dane źródłowe są publicznie dostępne w CEPiK. Ta strona jedynie je agreguje, analizuje i oblicza proste statystyki.</p>

    <h3>Widzę błędy w danych, dlaczego i jak to zgłosić?</h3>
    <p>Wszystkie dane źródłowe pochodzą z bazy danych CEPiK, więc jeżeli są w nich jakieś błędy to przyczyną jest błędna rejestracja pojazdu.</p>
    <p>Częste błędy jakie się pojawiają to np.:</p>
    <ul>
      <li>ta sama marka/producent samochodu pod dwoma nazwami (np. <i>Tesla Motors</i> vs <i>Tesla</i>),</li>
      <li>model samochodu zawarty w polu "marka" (np. <i>BMW I</i> vs <i>BMW</i>),</li>
      <li>wersja modelu zawarta w polu "model" (np. <i>Tesla Model S 75</i> vs <i>Tesla Model S</i>)</li>
    </ul>
    <p>Nie jest łatwo wyłapać te błędy, ponieważ strona zlicza wszystko automatycznie. Na etapie zbierania danych stworzona jest jednak pewna "normalizacja", która stara się takie błędy jak te podane wyżej naprawiać. Muszą one być jednak wcześniej zauważone przez człowieka i wpisane do programu jako poprawki, które należy w danych wprowadzić.</p>
    <p>Dlatego jeśli ktoś zauważy tego typu błędy to proszony jest o kontakt z autorem w celu ich poprawienia.</p>

    <h3>Kierowcy samochodów elektrycznych w Polsce?</h3>
    <p>Strona <i>CEPiK EV Stats</i> sygnowana jest logiem "Kierowcy samochodów elektrycznych w Polsce". Jest to grupa na Facebooku, która zrzesza właśnie takich kierowców i miłośników samochodów elektrycznych i jest świetnym miejscem do zapoznania się z tematem pojazdów elektrycznych. Zapraszamy nie tylko kierowców, ale też osoby, które przymierzają się do zakupu własnego EV.</p>

    <h3>Czy mogę zapoznać się z kodem źródłowym strony i programu?</h3>
    <p>Tak. Cały kod źródłowy dostępny jest w serwisie GitLab pod adresem: <ExtLink to='https://gitlab.com/michaldudek/cepik-ev-stats'>https://gitlab.com/michaldudek/cepik-ev-stats</ExtLink>. Ciekawskich zachęcam do zaznajomienia się z nim, ściągnięcia na komputer i pobawienia się we własnym zakresie, a także do zgłaszania uwag, sugestii czy (najlepiej) merge requestów.</p>
    <p>Szczegóły techniczne znajdują się w pliku README.md w repozytorium.</p>

    <h3>Jakie technologie zostały wykorzystane do stworzenia tej strony?</h3>
    <p>Strona powstała także po to, aby autor mógł odświeżyć sobie wiedzę z technologii webowych i zaznajomić się z najnowszymi trendami :)</p>
    <ul>
      <li>node.js</li>
      <li>TypeScript (backend)</li>
      <li>JavaScript / ECMAScript 2019</li>
      <li>ReactJS (16+) (całość oparta na hookach)</li>
      <li>CSS Modules + pluginy (np. custom media queries, nesting)</li>
      <li>GitLab CI - do uruchamiania kodu odpowiedzialnego za pobieranie danych z CEPiK i obliczania statystyk oraz publikowania strony</li>
      <li>GitLab Pages - do hostowania strony</li>
    </ul>

    <hr />
    
    <h2>O Autorze</h2>
    <img
      className={style.imageRight}
      src={`${process.env.PUBLIC_URL}/model3pl.jpg`}
      alt='Michał Pałys-Dudek i jego Tesla Model 3'
    />
    <p>Nazywam się Michał Pałys-Dudek i mieszkam we Wrocławiu.</p>
    <p>Z zawodu i pasji jestem programistą webowym (full stack) od kilkunastu lat, a także miłośnikiem samochodów elektrycznych.</p>
    <p>Na codzień jeżdżę czerwoną Teslą Model 3 Long Range AWD i prowadzę kanał na YouTube poświęcony właśnie Tesli - <ExtLink to='https://www.teslamodel3.pl'>Tesla Model 3 w Polsce</ExtLink>.</p>
    <p>Znaleźć mnie można na:</p>
    <ul>
      <li><ExtLink to='https://www.youtube.com/channel/UCLJlXUKQjT7L1c_Dbngoe3g'>YouTube</ExtLink></li>
      <li><ExtLink to='https://www.facebook.com/model3polska/'>Facebook</ExtLink></li>
      <li><ExtLink to='https://www.instagram.com/model3pl/'>Instagram</ExtLink></li>
      <li><ExtLink to='https://twitter.com/michaldudek'>Twitter</ExtLink></li>
      <li><ExtLink to='https://www.linkedin.com/in/michalpalysdudek/'>LinkedIn</ExtLink></li>
      <li><a href='mailto:michal@palys-dudek.pl?subject=CEPiK'>Email</a></li>
    </ul>
  </Container>
)
