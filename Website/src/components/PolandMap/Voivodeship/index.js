import React from 'react'
import PropTypes from 'prop-types'

import { voivodeships } from '../config'

import style from '../polandMap.module.css'

const Voivodeship = ({ id, path, color, onMouseOver, onClick }) => (
  <path
    className={style.voivodeship}
    d={path}
    fill={color}
    onClick={(ev) => onClick(ev, id)}
    onMouseOver={(ev) => onMouseOver(ev, id)}
  />
)

export default Voivodeship

Voivodeship.propTypes = {
  id: PropTypes.oneOf(Object.keys(voivodeships)).isRequired,
  path: PropTypes.string.isRequired,
  color: PropTypes.string,
  onClick: PropTypes.func,
  onMouseOver: PropTypes.func,
}

Voivodeship.defaultProps = {
  color: '#6c757d',
  onClick: () => null,
  onMouseOver: () => null,
}
