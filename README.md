CEPiK EV Stats
==============

Statystyki na temat pojazdów elektrycznych w Polsce na podstawie danych z Centralnej Ewidencji Pojazdów i Kierowców.

[CEPiK EV Stats](https://michaldudek.gitlab.io/cepik-ev-stats)

Strona składa się z dwóch aplikacji:

- `Collector` - odpowiedzialny za pobieranie, agregację i obliczanie statystyk z danych z publicznego API Centralnej Ewidencji Pojazdów i Kierowców
- `Website` - strona www prezentująca dane i statystyki na wykresach, zbudowana w React

## Collector

Technologie: `TypeScript`, `node.js`

Pobiera dane z CEPiK API i zapisuje do plików CSV oraz jednego głównego pliku JSON, który zawiera informacje o wszystkich wygenerowanych zestawach danych.

  $ npm i
  $ npm start

Komenda `$ npm start` rozpocznie pobieranie danych z API. Można ją konfigurować:

- `--from=YYYYMMDD` - data (w formacie `YYYYMMDD`), od której mają być pobierane dane z CEPiK
- `--output-dir` - folder, do którego mają być zapisane wszystkie pliki z danymi (domyślnie `../data/`)
- `--step` - wielkość okresu o jaki ma być odpytywane API CEPiK - `y [rok] / Q [kwartał] / M [miesiąc] / w [tydzień]`

Przykład użycia:

  $ npm start --  --from=20190101 --output-dir=../../data/

## Website

Technologie: `JavaScript / ECMAScript 2019`, `React`, `CSS Modules`, `Chart.js`

Strona WWW prezentująca dane i statystyki stworzone przez `Collector`a.

  $ npm i
  $ npm start

Komenda `$ npm start` uruchomi serwer deweloperski pod adresem `localhost:3000` i otworzy okno przeglądarki z tą stroną.

Projekt oparty jest o [Create React App](https://create-react-app.dev/), a następnie nadpisany przez [React App Rewired](https://github.com/timarney/react-app-rewired) w celu dodania kilku pluginów.

## Hosting

Strona deployowana jest w usłudze [GitLab Pages](https://about.gitlab.com/stages-devops-lifecycle/pages/), a więc musi być całkowicie wygenerowana do plików statycznych. Nie ma swojej bazy danych ani API.

### Deployment

Do deploymentu i uruchamiania `Collector`a wykorzystywana jest usługa [GitLab CI](https://gitlab.com/michaldudek/cepik-ev-stats/pipelines).

Plik `.gitlab-ci.yml` opisuje cały proces.

## Uwagi i Merge Requesty

Zachęcam do tworzenia issues, zgłaszania uwag i sugestii, a jeszcze bardziej do otwierania Merge Requestów. Każdy zostanie rozpatrzony w miarę czasu i możliwości :)

Proszę o zapoznanie się z kodem i utrzymywanie go w tej samej konwencji.
