import React from 'react'
import PropTypes from 'prop-types'
import { Doughnut } from 'react-chartjs-2'

import strings from 'config/strings'
import { DataShape } from 'config/types'
import { getColorForNumber } from 'config/colors'

const VehicleTypes = ({ data, size }) => {
  const labels = []
  const dataPoints = []
  const colors = []

  data.forEach((item, i) => {
    labels.push(item.key)
    dataPoints.push(item.count)
    colors.push(getColorForNumber(i))
  })

  return (
    <Doughnut
      height={size === 'half' ? 250 : null}
      data={{
        labels: labels,
        datasets: [{
          label: strings.countLabel,
          backgroundColor: colors,
          data: dataPoints,
        }]
      }}
    />
  )
}

export default VehicleTypes

VehicleTypes.propTypes = {
  data: PropTypes.arrayOf(DataShape).isRequired,
  size: PropTypes.oneOf(['full', 'half']),
}

VehicleTypes.defaultProps = {
  size: 'full',
}
