import React from 'react'
import PropTypes from 'prop-types'

import style from './tooltip.module.css'

const Tooltip = ({ position, name, label, color }) => (
  <div
    className={style.tooltip}
    style={{ ...position }}
  >
    <p className={style.name}>{name}</p>
    {label && (
      <p className={style.label}>
        {color && (
          <span
            className={style.color}
            style={{ backgroundColor: color }}
          />
        )}
        {label}
      </p>
    )}
  </div>
)

export default Tooltip

Tooltip.propTypes = {
  position: PropTypes.shape({
    top: PropTypes.number.isRequired,
    left: PropTypes.number.isRequired,
  }).isRequired,
  name: PropTypes.string.isRequired,
  label: PropTypes.string,
  color: PropTypes.string,
}
